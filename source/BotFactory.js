const TelegramBot = require('node-telegram-bot-api');
const config = require('./config.js');

module.exports = class BotFactory {
    Create() {
        const isRunningOnHeroku = (process.env.NODE && (~process.env.NODE.indexOf("heroku") != -1));

        let botOptions;
        const useWebHook = config.USE_WEB_HOOK && (!config.USE_WEB_HOOK_ONLY_ON_HEROKU || isRunningOnHeroku);
        if (useWebHook) {
            botOptions =
                {
                    webHook: {
                        port: process.env.PORT
                    }
                };
        }
        else {
            botOptions =
                {
                    polling: true
                };
        }

        const bot = new TelegramBot(config.SECURE.TELEGRAM_TOKEN, botOptions);

        console.log('isRunningOnHeroku: ' + isRunningOnHeroku);
        if (useWebHook) {
            if (isRunningOnHeroku) {
                const herokuUrl = process.env.APP_URL || ('https://' + config.HEROKU_APP_NAME + '.herokuapp.com:443');
                bot.setWebHook(`${herokuUrl}/bot${config.SECURE.TELEGRAM_TOKEN}`);

                console.log('WebHook started: ');
            }
            else {
                console.log('trying to start WebHook out of Heroku!');
            }
        }
        console.log('Polling is running: ' + bot.isPolling());
        bot.getWebHookInfo( function( webHookInfo )
        {
                console.log( 'WebhokInfo: ' + webHookInfo );
        } );
        return bot;
    }

    DefaultErrorHandler(error) {
        console.log('error code:' + error.code);
        if (error.code != 'EFATAL' && error.code != undefined ) {
            console.log('error body' + error.response.body);
        }
    }
};