const config = require('./config.js');

module.exports = 
{
    FORWARD_FROM_BOT : 'Я не зберігаю повідомлення від себе(бота)!',
    REPLY_TO_BOT : 'Я не зберігаю повідомлення від себе(бота) і не відповідаю на Reply!',
    REPLY_IN_PRIVATE_CHAT : 'Я не відповідаю на Reply в особистих чатах!',
    SAVED_MESSAGE_CHAT : 'Я зберіг повідомлення з конфи: ',
    SAVED_MESSAGE_CHAT_END : '. Його побачать у своїх збережених усі хто підписані на збережені цієї конфи.',
    SAVED_MESSAGE_PRIVATE : 'Я зберіг повідомлення від користувача: ',
    SAVED_MESSAGE_PRIVATE_END : '. У збережених його побачиш тільки ти.',
};
