const TelegramBot = require('node-telegram-bot-api');
const config = require('./config.js');
const localization = require('./localization.js');

module.exports = class SaverBotHandler {
    constructor() {
        this.bot = undefined;
        this.botUserId = undefined;
    }

    SetBot(newBot) {
        this.bot = newBot;

        var thisCopy = this;
        this.bot.getMe()
        .then( user => user )        
        .then( function(user)
        {
            thisCopy.botUserId = user.id;
        })
        .catch(function(err)
        {
            console.log("eror getting user id"+err);
        });
    }

    FilterMessage(msg) {
        //Since 'private mode' bots doen't receive messages with mentions in this lib,
        // filer them manually        
        //Allow mentions, replies and private msg
        if( msg.chat.type == 'private' )
        {
            return true;
        }
        if( msg.text != undefined )
        {
            if( msg.text.indexOf('@' + config.TELEGRAM_BOT_NAME) != -1 )
            {
                return true;
            }
        }
        if( msg.reply_to_message != undefined )
        {
            if( msg.reply_to_message.from.username == config.TELEGRAM_BOT_NAME )
            {
                return true;
            }
        }
        
        return false;
    }

    OnMessage(msg) {
        try
        {
            console.log('Unfiltered msg: "' + msg.text.substring(0,10) + '" chat.id="' + msg.chat.id + '"');
            if (!this.FilterMessage(msg))
            {
                return;
            }

            if( this.botUserId == undefined )
            {
                console.log("Error! botUserId still not loaded, msg processing cancelled.");
                return;
            }
            
            let readyForCommands = true;

            if( msg.reply_to_message != undefined )
            {
                if( msg.chat.type != 'private' )
                {                    
                    if( msg.reply_to_message.from.id == this.botUserId )
                    {
                        this.AnswerMessageWithText( msg, localization.REPLY_TO_BOT );                        
                    }
                    else
                    {
                        if( msg.reply_to_message.chat.id == msg.chat.id )
                        {
                            readyForCommands = false;

                            this.SaveMessageForAllSubscibers( msg.reply_to_message, msg.chat );
                            this.AnswerMessageWithText( msg,
                                 localization.SAVED_MESSAGE_CHAT + this.GetMessageOriginName(msg) + localization.SAVED_MESSAGE_CHAT_END );
                        }
                        else
                        {
                            readyForCommands = false;
                            
                            console.log('Error! Reply on chat message in other chat!!');
                        }
                    }
                }
                else
                {
                    this.AnswerMessageWithText( msg, localization.REPLY_IN_PRIVATE_CHAT );
                }
            }
            else if( msg.forward_from != undefined || msg.forward_from_chat != undefined )
            {                
                readyForCommands = false;//Deny forwarded commands

                if( msg.chat.type == 'private' )
                {
                    if( msg.forward_from.id == this.botUserId )
                    {
                        this.AnswerMessageWithText( msg, localization.FORWARD_FROM_BOT );
                    }
                    else
                    {
                        readyForCommands = false;
                            
                        this.SaveMessageForMe( msg ); 
                        this.AnswerMessageWithText( msg,
                            localization.SAVED_MESSAGE_PRIVATE + this.GetMessageOriginName(msg) + localization.SAVED_MESSAGE_PRIVATE_END );
                   
                    }
                }
                else
                {                         
                    console.log('Error! Received forwarded to group even after filtering');
                }
            }

            if( readyForCommands )
            {
                this.ParseCommands(msg);
            }
        }
        catch(err)
        {
            console.log('internal error on OnMessage: ' + err);
        }
    }

    AnswerMessageWithText( originalMsg, textToSend )
    {
        console.log('Answering with text:"' + textToSend + '"');
        this.bot.sendMessage(originalMsg.chat.id, textToSend);
    }

    GetMessageOriginName( msg )
    {
        if( msg.forward_from != undefined )
        {
            //for people forwards
            return msg.forward_from.first_name + ' ' + msg.forward_from.last_name;
        }
        else if( msg.forward_from_chat != undefined )
        {
            //for channels
            return msg.forward_from_chat.title;
        }
        else if( msg.reply_to_message != undefined )
        {
            //for replies in chat
            if( msg.chat.title != undefined )
            {
                return msg.chat.title;
            }
            else
            {
                console.log('Error! GetMessageOriginName called on private reply.');
                return 'ERROR';
            }
        }
    }

    ParseCommands( msg )
    {

    }

    SaveMessageForMe( msg )
    {
        const testChatId = 96267283;

        this.bot.forwardMessage(testChatId, msg.chat.id, msg.message_id );
    }

    
    SaveMessageForAllSubscibers( msg, associatedChat )
    {
        const testChatId = 96267283;

        this.bot.forwardMessage(testChatId, msg.chat.id, msg.message_id );
    }
};