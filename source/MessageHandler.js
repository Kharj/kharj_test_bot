const TelegramBot = require('node-telegram-bot-api');
const config = require('./config.js');

module.exports = class MessageHandler {
   constructor()
   {
       //Receive part
       this.allowedCommands = [];
       this.allowText = true;
       this.allowNumber = true;
       this.allowImage = true;
       this.allowAudio = true;
       this.OnReceive = function(conversation, message){};

       //Send part
       this.message = "";
       this.keyboardMarkup = undefined;
       this.ComposeMessage = this.DefaultComposeMessage();
   }

   DefaultComposeMessage( conversation )
   {
        return message;
   }
    
   SetOnReceive( callback )
   {
       this.OnReceive = callback;
   }

   SetKeyboardMarkup( map )
   {
       this.keyboardMarkup = map;
   }
   
   GetKeyboardMarkup()
   {
       return this.keyboardMarkup;
   }
};