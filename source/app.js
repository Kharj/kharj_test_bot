const TelegramBot = require('node-telegram-bot-api');
const config = require('./config.js');
const BotFactory = require('./BotFactory.js');
//const Conversation = require('./Conversation.js');
const SaverBotHandler = require('./SaverBotHandler.js');

var StatesEnum = Object.freeze({ "hello": 1, "howareyou": 2, "bye": 3 })

function main() {
    console.log('Staring bot...');
    const botFactory = new BotFactory();
    const bot = botFactory.Create();
    const saverBot = new SaverBotHandler();
    saverBot.SetBot(bot);

    bot.on('polling_error', botFactory.DefaultErrorHandler);
    bot.on('webhook_error', botFactory.DefaultErrorHandler);
    //JS is tricky regarding 'this', so just use extra lambda
    bot.on('message', (msg)=>{saverBot.OnMessage(msg)});

}

main();