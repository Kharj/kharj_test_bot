const configSecure = require('./configSecure.js');

module.exports = 
{
    TELEGRAM_BOT_NAME : 'kharj_test_bot',
    HEROKU_APP_NAME : 'kharj-test-bot',
	USE_WEB_HOOK: true,
    USE_WEB_HOOK_ONLY_ON_HEROKU: true,
    SECURE: configSecure,
};
